package dam.androidJavierSolerCanto.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class MyContacts {

    private ArrayList<ContactItem> myDataSet;
    private Context context;

    public MyContacts(Context context) {

        this.context = context;
        this.myDataSet = getContacts();
    }

    // Get contacts list from content provider
    private ArrayList<ContactItem> getContacts() {

        ArrayList<ContactItem> contactsList = new ArrayList<>();

        // Access to ContentProviders
        ContentResolver contentResolver= context.getContentResolver();

        // aux variables
        String[] projection = new String[] {
                ContactsContract.Data._ID,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.LOOKUP_KEY,
                ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI
        };

        String selectionFilter = ContactsContract.Data.MIMETYPE + "='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER + " IS NOT NULL";

        // query required data
        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                projection,
                selectionFilter,
                null,
                ContactsContract.Data.DISPLAY_NAME + " ASC");

        if (contactsCursor != null) {

            // get the column indexes for desired Name and Number columns

            // TODO Ex1: Añadimos los atributos a la consulta

            int idIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data._ID);
            int contactIdIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.CONTACT_ID);
            int lookupIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.LOOKUP_KEY);
            int rawIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int typeIndex = contactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int imageIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI);

            // read data and all to ArrayList
            while (contactsCursor.moveToNext()) {

                int id = contactsCursor.getInt(idIndex);
                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                int contactId = contactsCursor.getInt(contactIdIndex);
                String lookup = contactsCursor.getString(lookupIndex);
                int raw = contactsCursor.getInt(rawIndex);
                String type = contactsCursor.getString(typeIndex);
                Uri image;

                // TODO Ex1: Si no tiene una foto asociada le ponemos una por defecto

                if (contactsCursor.getString(imageIndex) != null) {
                    image = Uri.parse(contactsCursor.getString(imageIndex));

                } else {
                    String file = "android.resource://"+ context.getPackageName() + "/" + R.drawable.contact;
                    image = Uri.parse(file);
                }

                contactsList.add(new ContactItem(id,name,number,image,contactId,raw,type,lookup));
            }
            contactsCursor.close();
        }

        return contactsList;
    }

    public ContactItem getContactData(int position) {

        return myDataSet.get(position);
    }

    public int getCount() {

        return myDataSet.size();
    }
}
