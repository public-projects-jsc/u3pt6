package dam.androidJavierSolerCanto.u4t6contacts;

import android.net.Uri;

// TODO Ex1: Creamos la clase ContactItem con todos los atributos que cogemos en la consulta

public class ContactItem {

    private int id;
    private String name;
    private String number;
    private Uri image;
    private int contactId;
    private int rawContactId;
    private String phoneType;
    private String lookup;

    public ContactItem(int id, String name, String number, Uri image, int contactId, int rawContactId, String phoneType, String lookup) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.image = image;
        this.contactId = contactId;
        this.rawContactId = rawContactId;
        this.lookup = lookup;

        // Dependiendo del tipo (int) de teléfono será un tipo u otro

        switch (phoneType) {
            case "1":
                this.phoneType = "HOME";
                break;
            case "2":
                this.phoneType = "MOBILE";
                break;
            case "3":
                this.phoneType = "WORK";
                break;
            default:
                this.phoneType = "OTHER";
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Uri getImage() {
        return image;
    }

    public void setImage(Uri image) {
        this.image = image;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public int getRawContactId() {
        return rawContactId;
    }

    public void setRawContactId(int rawContactId) {
        this.rawContactId = rawContactId;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public String getLookup() {
        return lookup;
    }

    public void setLookup(String lookup) {
        this.lookup = lookup;
    }
}
