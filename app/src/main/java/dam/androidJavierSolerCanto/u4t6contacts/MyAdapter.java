package dam.androidJavierSolerCanto.u4t6contacts;

import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private MyContacts myContacts;
    private OnItemClickListener listener;
    private OnItemLongClickListener longListener;

    public interface OnItemClickListener {

        void onItemClick(ContactItem item);
    }

    public interface OnItemLongClickListener {

        void onItemLongClick(ContactItem item);
    }

    // Class for each item: contains only a TextView
    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvID;
        TextView tvName;
        TextView tvNumber;
        ImageView image;

        public  MyViewHolder(View view) {
            super(view);
            tvID = view.findViewById(R.id.tvID);
            tvName = view.findViewById(R.id.tvName);
            tvNumber = view.findViewById(R.id.tvNumber);
            image = view.findViewById(R.id.image);
        }

        // sets viewHolders views with data
        public void bind(final ContactItem contactData, final OnItemClickListener listener, final OnItemLongClickListener longListener) {
            tvID.setText(contactData.getId() + "");
            tvName.setText(contactData.getName());
            tvNumber.setText(contactData.getNumber());

            if (contactData.getImage() != null) {
                image.setImageURI(contactData.getImage());

            } else {
                image.setImageResource(R.drawable.contact);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(contactData);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    longListener.onItemLongClick(contactData);
                    return true;
                }
            });
        }
    }

    // cosntructor: myContacts contains Contacts data
    MyAdapter(MyContacts myContacts, OnItemClickListener listener, OnItemLongClickListener longListener) {
        this.myContacts = myContacts;
        this.listener = listener;
        this.longListener = longListener;
    }

    // Creates new view item: LayoutManager calls this method
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create item View:
        // use a simple TextView predefined layout () that contains only TextView
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);

        return new MyViewHolder(v);
    }

    // replaces the data content of a viewholder (recylces old viewholder): Layout Manager calls this method
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        // bind viewHolder with data at: position
        viewHolder.bind(myContacts.getContactData(position), listener, longListener);
    }

    // return the size of dataSet: Layout Manager Calls this method
    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }

}
